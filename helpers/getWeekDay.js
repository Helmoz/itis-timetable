export default function (date) {
  return (date || new Date())
    .toLocaleDateString('en-US', { weekday: 'long' })
    .toLowerCase()
}
