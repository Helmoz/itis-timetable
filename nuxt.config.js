export default {
  mode: 'spa',
  target: 'server',
  head: {
    title: 'ITIS Timetable',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Comfortaa:wght@400;700&display=swap',
      },
    ],
  },
  css: ['vuesax/dist/vuesax.css', '@/assets/main.css'],
  plugins: ['@/plugins/vuesax', '@/plugins/vueTouch'],
  components: true,
  buildModules: ['@nuxt/typescript-build', '@nuxtjs/pwa'],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/composition-api',
  ],
  axios: {},
  build: {},
  pwa: {
    meta: {
      name: 'ITIS Timetable',
    },
    manifest: {
      name: 'ITIS Timetable',
      short_name: 'Timetable',
      icons: [
        {
          "src": "/_nuxt/icons/icon_64x64.544738.png",
          "sizes": "64x64",
          "type": "image/png",
          "purpose": "any maskable"
        },
        {
          "src": "/_nuxt/icons/icon_120x120.544738.png",
          "sizes": "120x120",
          "type": "image/png",
          "purpose": "any maskable"
        },
        {
          "src": "/_nuxt/icons/icon_144x144.544738.png",
          "sizes": "144x144",
          "type": "image/png",
          "purpose": "any maskable"
        },
        {
          "src": "/_nuxt/icons/icon_152x152.544738.png",
          "sizes": "152x152",
          "type": "image/png",
          "purpose": "any maskable"
        },
        {
          "src": "/_nuxt/icons/icon_192x192.544738.png",
          "sizes": "192x192",
          "type": "image/png",
          "purpose": "any maskable"
        },
        {
          "src": "/_nuxt/icons/icon_384x384.544738.png",
          "sizes": "384x384",
          "type": "image/png",
          "purpose": "any maskable"
        },
        {
          "src": "/_nuxt/icons/icon_512x512.544738.png",
          "sizes": "512x512",
          "type": "image/png",
          "purpose": "any maskable"
        }
      ],
    },
    icon: {
      source: '[srcDir]/[staticDir]/myicon.png',
      fileName: 'myicon.png',
      sizes: [64, 120, 144, 152, 192, 384, 512]
    }
  },
}
