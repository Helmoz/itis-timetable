export default [
  {
    name: 'monday',
    shortName: 'ПН',
  },
  {
    name: 'tuesday',
    shortName: 'ВТ',
  },
  {
    name: 'wednesday',
    shortName: 'СР',
  },
  {
    name: 'thursday',
    shortName: 'ЧТ',
  },
  {
    name: 'friday',
    shortName: 'ПТ',
  },
  {
    name: 'saturday',
    shortName: 'СБ',
  },
]
