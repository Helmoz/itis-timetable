import getWeekDay from '@/helpers/getWeekDay'

export const state = () => ({
  active: getWeekDay(),
})

export const mutations = {
  setActive(state, payload) {
    state.active = payload
  },
}
